const mongoose = require("mongoose");
const getSecret = require("./secret");
const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const cors = require('cors');
const ShipEncounterData = require("./ship_encounter_data");

const API_PORT = 3001;
const app = express();
const router = express.Router();

mongoose.set('useFindAndModify', false); //avoid deprecation warnings
mongoose.set('useUnifiedTopology', false); //avoid deprecatoin warnings
mongoose.connect(getSecret("dbUri"),  { useNewUrlParser: true }); //mongodb node.js driver rewrote tool to parse connection strings


let db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
// disable below before prod
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
app.use(logger("dev"));

router.get("/", (req, res) => {
  res.json({ message: "HELLO WORLD" });
});

router.get("/ship_encounter/getData", (req, res) => {
  ShipEncounterData.find((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});

router.post("/ship_encounter/updateData", (req, res) => {
  const { id, update } = req.body;
  ShipEncounterData.findByIdAndUpdate(id, update, err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

router.delete("/ship_encounter/deleteData", (req, res) => {
  const { id } = req.body;
  ShipEncounterData.findByIdAndRemove(id, err => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

router.post("/ship_encounter/putData", (req, res) => {
  let data = new ShipEncounterData();

  const { state } = req.body;
  data.state = state;
  data.save(err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

app.use("/api", router); //this is added to the base url

app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));