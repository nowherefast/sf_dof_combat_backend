const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CrewDataSchema = new Schema(
  {
    characterId: String,
    characterName: String,
    playerName: String,
    baseAttack: String,
    ship_role: String,
    abilities:{
        strAbility_mod: Number,
        dexAbility_mod: Number,
        conAbility_mod: Number,
        intAbility_mod: Number,
        wisAbility_mod: Number,
        chaAbility_mod: Number
    },
    skills:{
        bluff_ranks: Number,
        bluff_class: Number,
        bluff_ability: Number,
        bluff_misc: Number,
        computers_ranks: Number,
        computers_class: Number,
        computers_ability: Number,
        computers_misc: Number,
        diplomacy_ranks: Number,
        diplomacy_class: Number,
        diplomacy_ability: Number,
        diplomacy_misc: Number,
        engineering_ranks: Number,
        engineering_class: Number,
        engineering_ability: Number,
        engineering_misc: Number,
        intimidate_ranks: Number,
        intimidate_class: Number,
        intimidate_ability: Number,
        intimidate_misc: Number,
        lifeScience_ranks: Number,
        lifeScience_class: Number,
        lifeScience_ability: Number,
        lifeScience_misc: Number,
        physicalScience_ranks: Number,
        physicalScience_class: Number,
        physicalScience_ability: Number,
        physicalScience_misc: Number,
        piloting_ranks: Number,
        piloting_class: Number,
        piloting_ability: Number,
        piloting_misc: Number
    }
  }
);

const ShipDataSchema = new Schema(
  {
    ship_id: Number,
    disposition: String,
    selected: Boolean,
    damage:{
        weapons_array: String,
        sensors: String,
        life_support: String,
        engines: String,
        power_core: String
    },
    exposure:{
        basic_information: Boolean,
        defenses:  Boolean,
        weapons: {
            wpns_turrets: Boolean,
            wpns_forward: Boolean,
            wpns_port: Boolean,
            wpns_starboard: Boolean,
            wpns_aft: Boolean
        },
        special_abilities: {
            spec_ability_1: Boolean,
            spec_ability_2: Boolean,
            spec_ability_3: Boolean,
            spec_ability_4: Boolean,
            spec_ability_5: Boolean,
            spec_ability_6: Boolean
        },
        load: Boolean,
        other: Boolean
    },
    shields:{
        forward_shields_max: Number,
        forward_shields_cur: Number,
        starboard_shields_max: Number,
        starboard_shields_cur: Number,
        aft_shields_max: Number,
        aft_shields_cur: Number,
        port_shields_max: Number,
        port_shields_cur: Number
    },
    systems:{
        thrusters:String,
        armor:String,
        computer:String,
        sensors:String,
        defensive_countermeasures: String
    },
    weapons:{
        forward_weapons: String,
        starboard_weapons: String,
        aft_weapons: String,
        port_weapons: String,
        turret_weapons: String 
    },
    special_abilities:{
        spec_ability_name_1: String,
        spec_ability_desc_1: String,
        spec_ability_name_2: String,
        spec_ability_desc_2: String,
        spec_ability_name_3: String,
        spec_ability_desc_3: String,
        spec_ability_name_4: String,
        spec_ability_desc_4: String,
        spec_ability_name_5: String,
        spec_ability_desc_5: String,
        spec_ability_name_6: String,
        spec_ability_desc_6: String
    },
    hull:{
        hp_percentage: Number,
        hp_max: Number,
        hp_cur: Number,
        ac_fill: Number,
        tl_fill: Number,
        dt_fill: Number,
        ct_fill: Number
    },
    stats:{
        ship_tier: Number,
        ship_name: String,
        classification: String,
        make_and_model: String,
        crew_compliment: Number,
        size: String,
        speed: Number,
        maneuverability: String,
        drift_rating: Number,
        drift_engine: String,
        power_core: String,
        misc_systems: String,
        expansion_bays: String,
        modifiers: String,
        cargo: String
    },
    crew:[CrewDataSchema]
  }
);

const ShipEncounterDataSchema = new Schema(
{
  state: {
    encounter_name: String,
    showSaveEncounter: Boolean,
    showLoadEncounter: Boolean,
    showCreateShip: Boolean,
    showCreateCharacter: Boolean,
    showDamages: Boolean,
    alerts:{
        cantAddPlayerWithoutShip: Boolean,
        cantDownloadCharacterIfNoneExist: Boolean,
        cantDownloadShipIfNoneExist: Boolean
    },
    showModal:{
        captain: Boolean,
        engineer: Boolean,
        pilot: Boolean,
        gunner: Boolean,
        science_officer: Boolean
    },
    data:[[
            {axis: String, value: Number},
            {axis: String, value: Number},
            {axis: String, value: Number},
            {axis: String, value: Number}        
          ]],
    active_character: Number,
    active_ship: Number,
    ship_data:[ShipDataSchema]
  }
}
);

module.exports = mongoose.model("ShipEncounterData", ShipEncounterDataSchema);